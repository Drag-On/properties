
Readme
======

PlainProperties provides functionality to integrate properties files into C++ projects in a simple and convenient manner. It lets you define the structure of a properties file in code, and automatically generates an appropriate object. The values can be read from file or from command line at any time.

Idea
----

Libraries like Boost.PropertyTree typically store all the keys and values in a sort of tree. This is straight forward enough, however it makes accessing non-string properties unneccesarily complex. Really, it would be convenient to have a properties class along these lines:

    class Properties {
	    int foo = 3;
	    bool bar = false;
	    struct {
		    std::string baz = "";
		} group;
		bool read(std::string filename);
		void parse(int argc, char** argv);
    }
   
   We don't really want to manually implement read() and parse() though. This is where PlainProperties kicks in: It generates this functionality without effort from the developer's side.

Example
-------

It only takes a couple of lines to specify a new properties structure:

    #include "BaseProperties.h"
	PROPERTIES_DEFINE(Test,
	  PROP_DEFINE(int, foo, 42)
	  GROUP_DEFINE(group,
	    PROP_DEFINE_A(int, foobar, 23, -fb)
	  )
	)

This results in a class similar to this:

    class TestProperties {
    public:
	    int foo = 42;
	    struct group {
		    int foobar = 23;
		}
		bool read(std::string filename, properties::PropertyFormat format = properties::PropertyFormat::INFO);
		void parse(int argc, char** argv);
	}

It can be instantiated just like any other object and provides some useful functionality, such as reading in properties from file and command line, and also printing all properties and their values to a stream.

    int main(int argc, char** argv)
	{
	    TestProperties properties;
	    properties.read("data/prop.info");
	    properties.fromCmd(argc, argv);
	    std::cout << properties << std::endl;
	    return 0;
	}

The output, given that neither the file nor the command line specified any other values, would be:

> foo: 42
> 
> group.foobar: 23

Usage
-----

### Defining a properties class
There are 3 macros you need to know about: PROPERTIES_DEFINE, PROP_DEFINE, and GROUP_DEFINE. Additionally, there is an extended version of PROP_DEFINE, called PROP_DEFINE_A, and an auxilliary macro ARG.

#### PROPERTIES_DEFINE(name, ...)
Defines a new properties class. The first argument is the name of the properties structure. The actual name of the class will then be *name*Properties. As second argument, any number of calls to PROP_DEFINE, PROP_DEFINE_A, and GROUP_DEFINE can be used.
The structure of the resulting class is documented further below.

#### PROP_DEFINE(type, name, dflt)
Defines a new property of type *type*, named *name*. It will be assigned *dflt* as default value.
This will be mapped to a class member variable.

#### PROP_DEFINE_A(type, name, dflt, arg)
Has the same functionality as PROP_DEFINE, but also gives the option to supply a short name to be used as an argument. Note that hyphens should be included in *arg*, as the macro uses it as-is.

#### GROUP_DEFINE(name, ...)
Defines a new group with name *name*. This will be mapped to a class member struct.

#### ARG(...)
This must be used to define types or default values that contain commas. Otherwise the comma interferes with the macro argument list.

### Class Structure
A class defined via PROPERTIES_DEFINE contains, except for some internal functionality, three useful methods:

#### bool read(std::string const& filename,  properties::PropertyFormat format = properties::PropertyFormat::INFO)
Reads in values from the file at *filename*, which is formatted according to *format*.
It returns true in case the file could be opened correctly, and false otherwise. Note that, only because the method returns true, it does not mean that any value has actually been read from the file.
The *format* parameter can be one of XML, JSON, INI, INFO. Note that some of these formats can express structures that can not be mapped to member variables or structs. These structures will be ignored.

#### bool fromCmd(int argc,  char** argv)
Parses command line arguments and copies the values to the approriate member variables. Note that you need to use DEFINE_PROP_A for this to work.

#### std::ostream& operator<<(std::ostream& stream, *name*Properties const& props)
Prints all properties and their values to the specified stream.

### More Exotic Datatypes
Per default, PlainProperties tries to parse values using operator<<. This works well for the fundamental types, but might not be sufficient. If you need something more exotic, you can define two methods in *namespace properties*:

#### void parse(std::string const& s, YourType& out)
Parses the string *s* and writes the result to *out*.

#### void toString(YourType const& var, std::string& out)
Formats *var* as a string and writes it to *out*.

PlainProperties already contains these methods for the STL types std::string, std::vector, and std::pair.

Dependencies
------------

PlainProperties relies on Boost.PropertyTree to read in files. No other libraries are needed.

Portability
-----------

As the standard doesn't specify how classes are layed out in memory (except for standard layout classes), this implementation is not generally portable. If the compiler decides to lay out the generated class in a non-straight-forward way, then PlainProperties will break. However, as the generated classes are non-polymorphic, there is no reasonable reason for a compiler to do so. I yet have to encounter a compiler where this implementation doesn't work.

License
-------

You may use this for whatever you like ;)