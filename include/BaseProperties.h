//
// Created by jan on 16.08.16.
//

#ifndef PROPERTIES_IPROPERTIES_H
#define PROPERTIES_IPROPERTIES_H

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <iostream>
#include <map>
#include <typeindex>
#include <type_traits>
#include <utility>
#include <sstream>

/**
 * Use this to wrap any argument that has commas in it
 */
#define ARG(...) __VA_ARGS__

/**
 * Defines a new properties class
 * @param name The name of the declared type will be \p name + Properties
 * @details The arguments after the first one can be used to add properties and groups. The class has a read() method
 *          which can be used to load properties from a file. It also has a fromCmd() method to parse command line
 *          arguments.
 * @example PROPERTIES_DEFINE(Default,
 *              PROP_DEFINE(int, foo, 42)
 *              PROP_DEFINE(bool, bar, true)
 *          )
 */
#define PROPERTIES_DEFINE(name, ...) \
    struct name##Properties \
    { \
        using SelfType = name##Properties; \
        private: \
            template <typename T> \
            struct Internal \
            { \
                static std::vector<boost::any> s_props; \
                static properties::TypeReadFunMap s_typeReadMap; \
                static properties::TypeStringFunMap s_typeStringMap; \
                static properties::ArgFunMap s_argMap; \
                static std::vector<std::string> s_groupIdentifier; \
            }; \
            Internal<void> internal; \
            SelfType* m_basePtr = this; \
            friend std::ostream& operator<< (std::ostream& out, SelfType const& prop); \
        public: \
            __VA_ARGS__ \
        public: \
            bool read(std::string const& filename, \
                      properties::PropertyFormat format = properties::PropertyFormat::INFO) \
            { \
                return properties::read(filename, format, Internal<void>::s_props, Internal<void>::s_typeReadMap, this); \
            } \
            void fromCmd(int argc,  char** argv) \
            { \
                properties::parseCmd(argc, argv, Internal<void>::s_argMap, this); \
            } \
    }; \
    template<typename T> std::vector<boost::any> name##Properties::Internal<T>::s_props; \
    template<typename T> properties::TypeReadFunMap name##Properties::Internal<T>::s_typeReadMap; \
    template<typename T> properties::TypeStringFunMap name##Properties::Internal<T>::s_typeStringMap; \
    template<typename T> properties::ArgFunMap name##Properties::Internal<T>::s_argMap; \
    template<typename T> std::vector<std::string> name##Properties::Internal<T>::s_groupIdentifier; \
    inline std::ostream& operator<<(std::ostream& stream, name##Properties const& props) \
    { \
        auto const& sProps = name##Properties::Internal<void>::s_props; \
        for(auto iter = sProps.begin(); iter != sProps.end(); ++iter) \
        { \
            auto const& d = *iter; \
            std::string propName, propValue; \
            name##Properties::Internal<void>::s_typeStringMap[std::type_index(d.type())](d, propName, propValue, &props); \
            stream << propName << ": " << propValue; \
            if(std::distance(iter, sProps.end()) > 1) \
                stream << std::endl; \
        } \
        return stream; \
    } \

/**
 * Defines a property which can be passed in via command-line argument
 * @param type Type of the property
 * @param name Name of the property
 * @param dflt Default value of the property
 * @param arg Command line argument name
 */
#define PROP_DEFINE_A(type, name, dflt, arg) \
    public: \
    type name = dflt; \
    private: \
    properties::AutoRegistration<type> name##Registration{Internal<void>::s_props, Internal<void>::s_typeReadMap, \
        Internal<void>::s_typeStringMap, Internal<void>::s_argMap, \
        properties::PropertyData<type>( \
            properties::buildQualifier(Internal<void>::s_groupIdentifier) + #name, \
            static_cast<size_t>(properties::byteOffset(m_basePtr, &name)), \
            #arg \
        )};

/**
 * Defines a property
 * @param type Type of the property
 * @param name Name of the property
 * @param dflt Default value of the property
 */
#define PROP_DEFINE(type, name, dflt) PROP_DEFINE_A(ARG(type), name, ARG(dflt),)

/**
 * Defines a group
 * @param name Name of the group
 * @details The arguments after the first one can be used to add properties and groups.
 */
#define GROUP_DEFINE(name, ...) \
    public: \
    struct name##Group \
    { \
        private: \
        SelfType* m_basePtr; \
        properties::GroupRegistration name##Registration{Internal<void>::s_groupIdentifier, #name}; \
        public: \
        name##Group(SelfType* basePtr) : m_basePtr(basePtr) { } \
        __VA_ARGS__ \
        private: \
        properties::GroupDeregistration name##Deregistration{Internal<void>::s_groupIdentifier}; \
    } name {this->m_basePtr};

/**
 * Contains functionality that belongs to the Properties implementation
 */
namespace properties
{
    /**
     * Type of an internal structure that maps types to functions that know how to deal with that type
     */
    using TypeReadFunMap = std::map<std::type_index, std::function<void(boost::any const&,
                                                                        boost::property_tree::ptree const&, void*)>>;

    /**
     * Type of the internal structure that maps types to functions that know how to stringize them
     */
    using TypeStringFunMap = std::map<std::type_index, std::function<void(boost::any const&, std::string&, std::string&,
                                                                          void const*)>>;

    /**
     * Type of the internal structure that maps an argument name to a function that knows how to deal with it
     */
    using ArgFunMap = std::map<std::string, std::function<void(std::string const&, void*)>>;

    /**
     * Known file formats
     */
    enum class PropertyFormat
    {
        XML, //< XML file format
        JSON, //< JSON file format
        INI, //< INI file format
        INFO, //< INFO file format
    };

    /**
     * Splits a string by a delimiter
     * @param s String to split
     * @param delim Delimiter to use
     * @return A vector with all the tokens
     */
    std::vector<std::string> split(std::string const& s, char delim);

    /**
     * Helper class that can be used to determine whether a class has operator<< defined
     */
    template<typename S, typename T>
    class is_left_streamable
    {
        template<typename SS, typename TT>
        static auto test(int) -> decltype(std::declval<SS&>() << std::declval<TT>(), std::true_type());

        template<typename, typename>
        static auto test(...) -> std::false_type;

    public:
        static const bool value = decltype(test<S, T>(0))::value;
    };

    /**
     * Helper class that can be used to determine whether a class has operator>> defined
     */
    template<typename S, typename T>
    class is_right_streamable
    {
        template<typename SS, typename TT>
        static auto test(int) -> decltype(std::declval<SS&>() >> std::declval<TT>(), std::true_type());

        template<typename, typename>
        static auto test(...) -> std::false_type;

    public:
        static const bool value = decltype(test<S, T>(0))::value;
    };

    /**
     * Generic implementation of a string parser. It uses the stream operator to convert the string to the specified
     * type. This might fail if the datatype doesn't provide such an implementation.
     * @param s String to parse
     * @param out Result
     */
    template<typename T>
    void parse(std::string const& s, T& out)
    {
        static_assert(is_right_streamable<std::stringstream, T&>::value,
                      "In order to use this type with the properties class, it must either define operator>> for stringstream"
                              " or you need to inject an approriate parse(std::string const&, T&) into namespace properties");

        std::stringstream stream (s);
        stream >> out;
    }

    /**
     * Simply copies over a string value
     * @param s String to parse
     * @param out Result
     */
    template<>
    void parse<std::string>(std::string const& s, std::string& out);

    /**
     * Parses a string and interprets it as a boolean
     * @param s String to parse
     * @param out Result
     */
    template<>
    void parse<bool>(std::string const& s, bool& out);

    /**
     * Parses a string and interprets it as a pair, split by comma.
     * @param s String to parse
     * @param out Result
     */
    template<typename T, typename U>
    void parse(std::string const& s, std::pair<T, U>& out)
    {
        auto tokens = split(s, ',');
        assert(tokens.size() == 2);

        parse(tokens[0], out.first);
        parse(tokens[1], out.second);
    }

    /**
     * Parses a string and interprets it as a vector, split by comma.
     * @param s String to parse
     * @param out Result
     */
    template<typename T>
    void parse(std::string const& s, std::vector<T>& out)
    {
        auto tokens = split(s, ',');
        out.resize(tokens.size());

        for(size_t i = 0; i < tokens.size(); ++i)
            parse(tokens[i], out[i]);
    }

    /**
     * Parses a string and interprets it as array, split by comma.
     * @param s String to parse
     * @param out Result
     */
    template<typename T, size_t S>
    void parse(std::string const& s, std::array<T, S>& out)
    {
        auto tokens = split(s, ',');

        for(size_t i = 0; i < tokens.size(); ++i)
            parse(tokens[i], out[i]);
    }

    /**
     * Tries to convert a value of any type to a string representation
     * @param var Value to convert
     * @param out Output string
     */
    template<typename T>
    void toString(T const& var, std::string& out)
    {
        static_assert(is_left_streamable<std::ostringstream, T>::value,
                      "In order to use this type with the properties class, it must either define operator<< for ostringstream"
                              " or you need to inject an approriate toString(T, std::string&) into namespace properties");

        std::ostringstream oss;
        oss << var;
        out = oss.str();
    }

    /**
     * Converts bool to a string representation
     * @param val Value to convert
     * @param out Output string
     */
    template<>
    void toString<bool>(bool const& val, std::string& out);

    /**
     * Converts std::pair to a string representation
     * @param pair Pair to convert
     * @param out Output string
     */
    template<typename T, typename U>
    void toString(std::pair<T, U> const& pair, std::string& out)
    {
        std::string first, second;
        toString(pair.first, first);
        toString(pair.second, second);
        out = "( " + first + ", " + second + " )";
    }

    /**
     * Converts std::vector to a string representation
     * @param vec Vector to convert
     * @param out Output string
     */
    template<typename T>
    void toString(std::vector<T> const& vec, std::string& out)
    {
        out = "{ ";
        for (size_t i = 0; i < vec.size() - 1; ++i)
        {
            std::string s;
            toString(vec[i], s);
            out += s + ", ";
        }
        if (!vec.empty())
        {
            std::string s;
            toString(vec.back(), s);
            out += s;
        }
        out += " }";
    }

    /**
     * Converts std::array to a string representation
     * @param arr Array to convert
     * @param out Output string
     */
    template<typename T, size_t S>
    void toString(std::array<T, S> const& arr, std::string& out)
    {
        out = "( ";
        for(size_t i = 0; i < S - 1; ++i)
        {
            std::string content;
            toString(arr[i], content);
            out += content + ", ";
        }
        std::string content;
        toString(arr[S-1], content);
        out += content + " )";
    }

    /**
     * Computes the offset between two pointers (in bytes)
     */
    template <typename T, typename U>
    long int byteOffset(T const* base_ptr, U const* child_ptr)
    {
        return reinterpret_cast<uint8_t const*>(child_ptr) - reinterpret_cast<uint8_t const*>(base_ptr);
    }

    /**
     * Reads a properties file and copies over all properties in a certain Properties object
     * @param filename File to read from
     * @param format File format
     * @param props Properties meta data
     * @param typeReadMap Read map
     * @param basePtr Properties base pointer
     * @return True in case everything went right, otherwise false
     */
    bool read(std::string const& filename, properties::PropertyFormat format, std::vector<boost::any> const& props,
              TypeReadFunMap& typeReadMap, void* basePtr);

    /**
     * Parses command line arguments
     * @param argc Amount of arguments
     * @param argv Argument values
     * @param argMap Argument map
     * @param basePtr Properties class base ptr
     */
    void parseCmd(int argc, char** argv, ArgFunMap const& argMap, void* basePtr);

    /**
     * Internal structure that stores meta information about member variables of a properties class
     * @tparam T Type of the member variable
     */
    template<typename T>
    class PropertyData
    {
    public:
        using Type = T;
        std::string m_name;
        std::string m_short;
        size_t m_offset;

        PropertyData(std::string const& name, size_t offset, std::string const& shrt = "")
                : m_name(name),
                  m_short(shrt),
                  m_offset(offset)
        {}
    };

    /**
     * Internal structure that stores the metadata of a new property
     * @tparam T Type of the member variable
     */
    template<typename T>
    struct AutoRegistration
    {
        AutoRegistration(std::vector<boost::any>& props, TypeReadFunMap& typeReadFunMap,
                         TypeStringFunMap& typeStringFunMap, ArgFunMap& argFunMap, PropertyData<T> data)
        {
            props.emplace_back(data);

            // Emplace a function that is used whenever this property is overwritten via command line arguments
            size_t idx = props.size() - 1;
            auto argFun = [&props, idx](std::string const& argval, void* basePtr)
            {
                auto const& e = props[idx];
                PropertyData<T> p = boost::any_cast<PropertyData<T>> (e);
                T* actualVar = reinterpret_cast<T*>(reinterpret_cast<uint8_t*>(basePtr) + p.m_offset);
                properties::parse(argval, *actualVar);
            };
            argFunMap[data.m_short] = argFun;

            // Emplace function that is called whenever a property of this type is read from a file
            if (typeReadFunMap.count(std::type_index(typeid(T))) == 0)
            {
                auto typeReadFun = [](boost::any const& e, boost::property_tree::ptree const& prop, void* basePtr)
                {
                    PropertyData<T> p = boost::any_cast<PropertyData<T>> (e);
                    T* actualVar = reinterpret_cast<T*>(reinterpret_cast<uint8_t*>(basePtr) + p.m_offset);
                    std::string str = prop.get(p.m_name, "");
                    properties::parse(str, *actualVar);
                    //std::cout << "Assigned " << p.m_name << std::endl;
                };
                typeReadFunMap[std::type_index(typeid(PropertyData<T>))] = typeReadFun;
            }

            // Emplace function that is called whenever a property of this type is printed to the screen
            if(typeStringFunMap.count(std::type_index(typeid(T))) == 0)
            {
                auto typeStringFun = [](boost::any const& e, std::string& name, std::string& value, void const* basePtr)
                {
                    PropertyData<T> p = boost::any_cast<PropertyData<T>> (e);
                    T const* actualVar = reinterpret_cast<T const*>(reinterpret_cast<uint8_t const*>(basePtr) + p.m_offset);
                    name = p.m_name;
                    properties::toString(*actualVar, value);
                };
                typeStringFunMap[std::type_index(typeid(PropertyData<T>))] = typeStringFun;
            }
        }
    };

    /**
     * Internal structure that registers a new group
     */
    struct GroupRegistration
    {
        GroupRegistration(std::vector<std::string>& groupIds, std::string const& name)
        {
            groupIds.push_back(name);
        }
    };

    /**
     * Internal structure that deregisters the last registered group
     */
    struct GroupDeregistration
    {
        GroupDeregistration(std::vector<std::string>& groupIds)
        {
            groupIds.pop_back();
        }
    };

    /**
     * Builds up a string that contains the group hierarchy up to the current variable
     * @param stack Current group hierarchy
     * @return The string containing the group hierarchy, connected with a dot "."
     */
    std::string buildQualifier(std::vector<std::string> const& stack);
}

#endif //PROPERTIES_IPROPERTIES_H
