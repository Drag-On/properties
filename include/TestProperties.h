//
// Created by jan on 16.08.16.
//

#ifndef PROPERTIES_TESTPROPERTIES_H
#define PROPERTIES_TESTPROPERTIES_H

#include "BaseProperties.h"

PROPERTIES_DEFINE(Test,
  PROP_DEFINE_A(int, foo, 42, -f)
  PROP_DEFINE_A(bool, bar, false, -b)
  PROP_DEFINE(int, baz, 300)
  GROUP_DEFINE(group1,
    PROP_DEFINE_A(int, foobar, 23, -fb)
  )
  GROUP_DEFINE(group2,
    PROP_DEFINE(std::vector<std::string>, blub, {"yeeehaw"})
    GROUP_DEFINE(group3,
      PROP_DEFINE_A(std::string, knowledge, "idk", -k)
      PROP_DEFINE(ARG(std::pair<int,float>), milkPerGlass, ARG({1,3.4f}))
    )
  )
)

#endif //PROPERTIES_TESTPROPERTIES_H
