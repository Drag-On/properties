#include <iostream>
#include "TestProperties.h"

int main(int argc, char** argv)
{
    std::cout << "Default values:" << std::endl;

    TestProperties properties;
    std::cout << properties << std::endl;
    std::cout << std::endl << "After reading file:" << std::endl;
    properties.read("data/prop.info", properties::PropertyFormat::INFO);
    properties.fromCmd(argc, argv);
    std::cout << properties << std::endl;

    return 0;
}