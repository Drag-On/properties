//
// Created by jan on 16.08.16.
//

#include "BaseProperties.h"

std::string properties::buildQualifier(std::vector<std::string> const& stack)
{
    std::string qualifier;
    for(auto const& s : stack)
        qualifier += s + ".";
    return qualifier;
}

std::vector<std::string> properties::split(std::string const& s, char delim)
{
    std::vector<std::string> result;
    std::stringstream sstream(s);
    std::string token;
    while (std::getline(sstream, token, delim))
        result.push_back(token);
    return result;
}

template<>
void properties::parse<bool>(std::string const& s, bool& out)
{
    if (s == "true" || s == "on")
    {
        out = true;
        return;
    }
    else if (s == "false" || s == "off")
    {
        out = false;
        return;
    }
    else
    {
        std::stringstream stream(s);
        stream >> std::boolalpha >> out;
        return;
    }
}
template<>
void properties::parse<std::string>(std::string const& s, std::string& out)
{
    out = s;
}

template<>
void properties::toString<bool>(bool const& val, std::string& out)
{
    if (val)
        out = "true";
    else
        out = "false";
}

void properties::parseCmd(int argc, char** argv, properties::ArgFunMap const& argMap, void* basePtr)
{
    for (int i = 0; i < argc; ++i)
    {
        if (argMap.count(argv[i]) > 0)
        {
            std::string argval = "";
            if (argc > i + 1)
                argval = argv[i + 1];
            argMap.at(argv[i])(argval, basePtr);
        }
    }
}

bool
properties::read(std::string const& filename, properties::PropertyFormat format, std::vector<boost::any> const& props,
                   properties::TypeReadFunMap& typeReadMap, void* basePtr)
{
    try
    {
        boost::property_tree::ptree propTree;
        switch (format)
        {
            case properties::PropertyFormat::XML:
                boost::property_tree::read_xml(filename, propTree);
                break;
            case properties::PropertyFormat::JSON:
                boost::property_tree::read_json(filename, propTree);
                break;
            case properties::PropertyFormat::INI:
                boost::property_tree::read_ini(filename, propTree);
                break;
            case properties::PropertyFormat::INFO:
                boost::property_tree::read_info(filename, propTree);
                break;
        }
        for (auto const& d : props)
            typeReadMap[std::type_index(d.type())](d, propTree, basePtr);
        return true;
    }
    catch (std::exception& e)
    {
        std::cerr << "Can't open properties file: \"" << e.what() << "\"." << std::endl;
    }
    catch (...)
    {
        std::cerr << "Can't open properties file." << std::endl;
    }
    return false;
}
